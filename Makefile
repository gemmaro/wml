url = https://www.w3.org/People/Raggett/wml.html
doc = wml.html
xdoc = wml.xhtml

translate: ${xdoc} link_to_original.add public
	po4a po4a.cfg

public:
	mkdir $@

${xdoc}: ${doc}
	ruby to_xhtml.rb

${doc}:
	curl -o $@ ${url}
